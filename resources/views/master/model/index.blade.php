@extends('layouts.header')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Model</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-3">
                                <form action="{{ url('/model/save') }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="model">Model</label>
                                        <input type="text" name="model" class="form-control {{ $errors->has('model') ? 'is-invalid':'' }}">
                                        <p class="text-danger">{{ $errors->first('model') }}</p>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary btn-sm"><i class="fa fa-paper-plane"></i> Simpan</button>
                                    </div>
                                </form>
                            </div>

                            <div class="col-md-9">
                                <table class="table table-hover table-bordered" style="width: 100%" id="table-model">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">No</th>
                                            <th>Model</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    $(function() {
        $('#table-model').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: 'model/json',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'model', name: 'model' },
                { data: 'Aksi', name: 'Aksi' }
            ]
        });
    });

    function myConfirm() {
        var result = confirm("Yakin ingin menghapus data tersebut ?");
        if (result==true) {
            return true;
        } else {
            return false;
        }
    }
</script>
@endpush