<div class="col-md-3 left_col">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a href="{{ url('/dashboard') }}" class="site_title"><i class="fa fa-twitter-square"></i> <span> Solata </span></a>
    </div>

    <div class="clearfix"></div>
    <!-- /menu profile quick info -->

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <ul class="nav side-menu">
          <li>
            <a><i class="fa fa-home"></i> Master <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{ url('/master_barang') }}">Barang</a></li>
              <li><a href="{{ url('/kategori') }}">Kategori</a></li>
              <li><a href="{{ url('/sub_kategori') }}">Sub Kategori</a></li>
              <li><a href="{{ url('/model') }}">Model</a></li>
              <li><a href="{{ url('/size') }}">Size</a></li>
              <li><a href="{{ url('/type') }}">Tipe</a></li>
              <li><a href="{{ url('/color') }}">Warna</a></li>
            </ul>
          </li>
          <li>
            <a><i class="fa fa-pencil-square-o"></i>Transaction <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{ url('/stok_barang') }}">Stok Barang</a></li>
              <li><a href="{{ url('/penjualan') }}">Penjualan</a></li>
            </ul>
          </li>
          <li>
            <a><i class="fa fa-bar-chart-o"></i>Report <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{ url('/penjualan/report') }}">Report Penjualan</a></li>
              <li><a href="{{ url('/penjualan/grafik') }}">Grafik</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  <!-- /sidebar menu -->
  </div>
</div>

<!-- top navigation -->
<div class="top_nav">
  <div class="nav_menu">
    <nav>
      <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
      </div>

      <ul class="nav navbar-nav navbar-right">
        @guest
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            <!-- @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
            @endif -->
        @else
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="user-profile dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        @endguest
      </ul>
    </nav>
  </div>
</div>
