<?php

namespace App\Http\Controllers\Master;

use App\Model\MColor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use Alert;

class MasterColorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master/color.index');
    }

    public function json(){
        $mc = MColor::query();
        $mc = $mc->select('*');
        return Datatables::of($mc)
            ->addColumn('Aksi', function($mc){
                return  '<a href="'. url('/color/'.$mc->id).'" style="margin:2px;" class="btn btn-warning btn-xs "><i class="fa fa-pencil"></i> Edit</a>'.
                    '<a onclick="return myConfirm();" href="'. url('/color/delete/'.$mc->id).'" style="margin:2px;" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i> Delete</a>';
            })->rawColumns(['Aksi'])->make(true); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,['color' => 'required']);

        try {

            // $kode = rand(0,9);
            $mc = MColor::create($request->all());

            Alert::success('Warna '.$mc->color.' berhasil ditambahkan', 'Success')->autoclose(4000);
            return redirect('/color');
        } catch (\Exception $e) {
            Alert::error('Error '.$e->getMessage(), 'Maaf!')->persistent('Close');
            return redirect('/color');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\MColor  $mColor
     * @return \Illuminate\Http\Response
     */
    public function show(MColor $mColor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\MColor  $mColor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mc = MColor::findOrFail($id);
        return view('master/color.edit',compact('mc'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\MColor  $mColor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,['color' => 'required']);

        try {
            $mc = MColor::findOrFail($id);
            $mc->update($request->all());

            Alert::success('Warna '.$mc->color.' berhasil diubah', 'Success')->autoclose(4000);
            return redirect('/color');
        } catch (\Exception $e) {
            Alert::error('Error '.$e->getMessage(), 'Maaf!')->persistent('Close');
            return redirect('/color/edit');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\MColor  $mColor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $mc = MColor::findOrFail($id);
            $mc->delete();

            Alert::success('Warna '.$mc->color.' berhasil dihapus', 'Success')->autoclose(4000);
            return redirect('/color');
        } catch (\Exception $e) {
            Alert::error('Error '.$e->getMessage(), 'Maaf!')->persistent('Close');
            return redirect('/color');      
        }
    }
}
