@extends('layouts.header')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Penjualan Barang</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <a class="close-link btn btn-sm btn-info" href="{{ url('/penjualan/create') }}" style="color: white;"><i class="fa fa-plus" title="Tambah Data"></i>Tambah Data</a>
                            </li>
                            <li>
                                <a class="collapse-link"><i class="fa fa-chevron-up" title="Tutup Layar"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">                           
                            <table class="table table-hover table-bordered" style="width: 100%" id="table-stok">
                                <thead>
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>Image</th>
                                        <th>Kode Stok Barang</th>
                                        <th>Nama Barang</th>
                                        <th>Merek</th>
                                        <th>Kategori</th>
                                        <th>Jumlah Barang</th>
                                        <th>Warna</th>
                                        <th>Model</th>
                                        <th>Tipe</th>
                                        <th>Ukuran</th>
                                        <!-- <th>Aksi</th> -->
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    $(document).ready(function (){
        $('#table-stok').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: 'penjualan/json',
            columns: [
                { data: 'created_at', name: 'created_at' },
                { data: 'image_path', name: 'image_path',
                    render:function(data,type,meta){
                        return '<img src="{{asset("uploads/images")}}'+"/"+''+data+'" width="100%" />';
                    }
                },
                { data: 'kode_stok_barang', name: 'kode_stok_barang' },
                { data: 'nama_barang', name: 'nama_barang' },
                { data: 'merek', name: 'merek' },
                { data: 'nama_kategori', name: 'nama_kategori' },
                { data: 'jumlah_barang', name: 'jumlah_barang' },
                { data: 'color', name: 'color' },
                { data: 'model', name: 'model' },
                { data: 'type', name: 'type' },
                { data: 'size', name: 'size' }
            ],
            columnDefs:[
                { "width": "30%", "targets": 1 }
            ]
        });
    });

    function myConfirm() {
        var result = confirm("Yakin ingin menghapus data tersebut ?");
        if (result==true) {
            return true;
        } else {
            return false;
        }
    }
</script>
@endpush