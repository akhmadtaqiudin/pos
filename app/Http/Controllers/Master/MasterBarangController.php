<?php

namespace App\Http\Controllers\Master;

use App\Model\Barang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Kategori;
use App\Model\MColor;
use App\Model\MModel;
use App\Model\MSize;
use App\Model\MType;
use Carbon\Carbon;
use DataTables;
use Alert;
use Image;
use File;

class MasterBarangController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master/barang.index');
    }

    public function json(){
        $barang = Barang::query();
        $barang = $barang->select('*');
        return Datatables::of($barang)
            ->addColumn('Aksi', function($barang){
                return  '<a href="'. url('/master_barang/'.$barang->id).'" style="margin:2px;" class="btn btn-warning btn-xs "><i class="fa fa-pencil"></i> Edit</a>'.
                    '<a onclick="return myConfirm();" href="'. url('/master_barang/delete/'.$barang->id).'" style="margin:2px;" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i> Delete</a>';
            })
            ->rawColumns(['Aksi'])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('master/barang.created');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'kode_barang' => 'required',
            'nama_barang' => 'required|max:100',
            'image' => 'required|image|mimes:jpg,png,jpeg'
        ]);

        try {

            $image = $request->file('image');

            $acak = rand(0, 99999);
            $kode = $request['kode_barang'];
            $kode = $kode.$acak;
            $fileName = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('uploads/images'), $fileName);

            $form_data = array(
                'kode_barang'       =>   $kode,
                'nama_barang'       =>   $request->nama_barang,
                'merek'             =>   $request->merek,
                'keterangan'        =>   $request->keterangan,
                'additional_data1'  =>   $request->additional_data1,
                'image_path'        =>   $fileName
            );

            Barang::create($form_data);

            Alert::success('Data '.$request->nama_barang.' Berhasil disimpan','Success')->autoclose(4000);
            return redirect('/master_barang');
        } catch (\Exception $e) {
            Alert::error('Error '.$e->getMessage(), 'Maaf !')->persistent('Close');
            return redirect('/master_barang/create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function show(Barang $barang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $m_barang = Barang::findOrFail($id);
        return view('master/barang.update', compact('m_barang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            $image_name = $request->hidden_image;
            $image = $request->file('image');
            if($image != '')
            {
                $request->validate([
                    'kode_barang' => 'required',
                    'nama_barang' => 'required|max:100',
                    'image'       => 'required|image|mimes:jpg,png,jpeg'
                ]);

                $image_name = rand() . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('uploads/images'), $image_name);
            }
            else
            {
                $request->validate([
                    'nama_barang'    =>  'required',
                    'nama_barang' => 'required|max:100'
                ]);
            }

            $form_data = array(
                'kode_barang'       =>   $request->kode_barang,
                'nama_barang'       =>   $request->nama_barang,
                'merek'             =>   $request->merek,
                'keterangan'        =>   $request->keterangan,
                'additional_data1'  =>   $request->additional_data1,
                'image_path'        =>   $image_name
            );
      
            Barang::whereId($id)->update($form_data);

            Alert::success('Data '.$request->nama_barang.' berhasil diubah', 'Success')->autoclose(4000);
            return redirect('/master_barang');
        } catch (\Exception $e) {
            Alert::error('Error '.$e->getMessage(), 'Maaf!')->persistent('Close');
            return redirect('/master_barang/'.$id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $barang = Barang::findOrFail($id);
            $barang->delete();

            Alert::success('Barang '.$barang->nama_barang.' berhasil dihapus', 'Success')->autoclose(4000);
            return redirect('/master_barang');
        } catch (\Exception $e) {
            Alert::error('Error '.$e->getMessage(), 'Maaf!')->persistent('Close');
            return redirect('/master_barang');      
        }
    }
}
