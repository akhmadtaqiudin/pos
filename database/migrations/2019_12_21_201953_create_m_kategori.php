<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMKategori extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_kategori', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_kategori');
            $table->string('nama_kategori');
            $table->string('additional_data1')->nullable();
            $table->string('additional_data2')->nullable();
            $table->integer('additional_data3')->nullable();
            $table->integer('additional_data4')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_kategori');
    }
}
