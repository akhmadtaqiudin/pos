<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MColor extends Model
{
    protected $table = 'm_color';
    protected $guarded = [];
    protected $hidden = ['created_at','updated_at'];
}
