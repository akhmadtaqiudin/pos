<?php

namespace App\Http\Controllers\Master;

use App\Model\MSize;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use Alert;

class MasterSizeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master/size.index');
    }

    public function json(){
        $ms = MSize::query();
        $ms = $ms->select('*');
        return Datatables::of($ms)
            ->addColumn('Aksi', function($ms){
                return  '<a href="'. url('/size/'.$ms->id).'" style="margin:2px;" class="btn btn-warning btn-xs "><i class="fa fa-pencil"></i> Edit</a>'.
                    '<a onclick="return myConfirm();" href="'. url('/size/delete/'.$ms->id).'" style="margin:2px;" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i> Delete</a>';
            })->rawColumns(['Aksi'])->make(true); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,['size' => 'required']);

        try {
            $ms = MSize::create($request->all());

            Alert::success('Size '.$ms->size.' berhasil ditambahkan', 'Success')->autoclose(4000);
            return redirect('/size');
        } catch (\Exception $e) {
            Alert::error('Error '.$e->getMessage(), 'Maaf!')->persistent('Close');
            return redirect('/size');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\MSize  $mSize
     * @return \Illuminate\Http\Response
     */
    public function show(MSize $mSize)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\MSize  $mSize
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ms = MSize::findOrFail($id);
        return view('master/size.edit',compact('ms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\MSize  $mSize
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,['size' => 'required']);

        try {
            $ms = MSize::findOrFail($id);
            $ms->update($request->all());

            Alert::success('Size '.$ms->size.' berhasil diubah', 'Success')->autoclose(4000);
            return redirect('/size');
        } catch (\Exception $e) {
            Alert::error('Error '.$e->getMessage(), 'Maaf!')->persistent('Close');
            return redirect('/size/edit');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\MSize  $mSize
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $ms = MSize::findOrFail($id);
            $ms->delete();

            Alert::success('Size '.$ms->size.' berhasil dihapus', 'Success')->autoclose(4000);
            return redirect('/size');
        } catch (\Exception $e) {
            Alert::error('Error '.$e->getMessage(), 'Maaf!')->persistent('Close');
            return redirect('/size');      
        }
    }
}
