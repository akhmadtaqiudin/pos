<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>{{ config('app.name', 'Toko') }}</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->        
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    </head>
    <body>
        <header>
            <nav class="navbar fixed-top navbar-dark bg-dark shadow-sm">
                <div class="container">
                    <a href="{{ url('/') }}" class="navbar-brand">
                        <img src="{{ asset('images/solata-2.png') }}" style="width: 58%;">
                    </a>

                    <form class="form-inline" action="{{ url('/search') }}" method="GET">
                        <input name="nama_barang" class="form-control mr-sm-2" type="text" placeholder="Nama Produk" aria-label="Nama Produk">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Cari</button>
                    </form>
                </div>            
            </nav>
        </header>

        <main role="main">       
            <div class="container">
                <div class="row">
                    <div class="co"></div>
                    <div class="co"></div>
                    @foreach($barang as $data)
                        <div class="col-sm-6 col-md-4 col-lg-3 mt-4">
                            <div class="card">
                                <a href="{{ url('/search/'.$data->kode_stok_barang) }}">
                                    <img class="card-img-top" src="{{ asset('uploads/images/'.$data->image_path) }}">
                                    <div class="card-body">
                                        <p class="card-text">{{ $data->nama_barang }}</p>
                                        <p class="card-text">Rp. {{ number_format($data->harga_jual, 2) }}</p>              
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </main>

        <footer class="text-muted">
            <div class="container">                
                <p class="float-right">
                    <a href="#">Back to top</a>
                </p>
            </div>
        </footer>
        
        <script src="{{ asset('js/jquery-3.4.1.slim.min.js') }}" ></script>
        <script src="{{ asset('js/popper.min.js') }}" ></script>
        <script src="{{ asset('js/bootstrap-4.4.1.min.js') }}" ></script>
    </body>
</html>
