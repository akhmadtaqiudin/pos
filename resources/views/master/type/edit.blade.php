@extends('layouts.header')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Edit Type</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li>
                                <a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form action="{{ url('/type/update/'.$mt->id) }}" method="POST"  class="form-horizontal">
                            @csrf
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="id">No </label>
                                    </div>
                                    <div class="col-md-9">
                                         <input type="text" name="id" value="{{ $mt->id }}" class="form-control" readonly="true">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="type">Type</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="type" value="{{ $mt->type }}" class="form-control {{ $errors->has('type') ? 'is-invalid':'' }}">
                                <p class="text-danger">{{ $errors->first('type') }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <a href="{{ url('/type') }}" class="btn btn-info btn-sm"><i class="fa fa-mail-reply"></i> Kembali</a>
                                <button class="btn btn-success btn-sm"><i class="fa fa-paper-plane"></i> Ubah</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
