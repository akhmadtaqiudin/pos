<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'm_kategori';
    protected $guarded = [];
    protected $hidden = ['created_at','updated_at'];
}
