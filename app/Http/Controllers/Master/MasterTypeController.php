<?php

namespace App\Http\Controllers\Master;

use App\Model\MType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use Alert;

class MasterTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master/type.index');
    }

    public function json(){
        $mt = MType::query();
        $mt = $mt->select('*');
        return Datatables::of($mt)
            ->addColumn('Aksi', function($mt){
                return  '<a href="'. url('/type/'.$mt->id).'" style="margin:2px;" class="btn btn-warning btn-xs "><i class="fa fa-pencil"></i> Edit</a>'.
                    '<a onclick="return myConfirm();" href="'. url('/type/delete/'.$mt->id).'" style="margin:2px;" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i> Delete</a>';
            })->rawColumns(['Aksi'])->make(true); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,['type' => 'required']);

        try {
            $mt = MType::create($request->all());

            Alert::success('Tipe '.$mt->type.' berhasil ditambahkan', 'Success')->autoclose(4000);
            return redirect('/type');
        } catch (\Exception $e) {
            Alert::error('Error '.$e->getMessage(), 'Maaf!')->persistent('Close');
            return redirect('/type');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\MType  $mType
     * @return \Illuminate\Http\Response
     */
    public function show(MType $mType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\MType  $mType
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mt = MType::findOrFail($id);
        return view('master/type.edit',compact('mt'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\MType  $mType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,['type' => 'required']);

        try {
            $mt = MType::findOrFail($id);
            $mt->update($request->all());

            Alert::success('Tipe '.$mt->type.' berhasil diubah', 'Success')->autoclose(4000);
            return redirect('/type');
        } catch (\Exception $e) {
            Alert::error('Error '.$e->getMessage(), 'Maaf!')->persistent('Close');
            return redirect('/type/edit');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\MType  $mType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) 
    {
        try {
            $mt = MType::findOrFail($id);
            $mt->delete();

            Alert::success('Tipe '.$mt->type.' berhasil dihapus', 'Success')->autoclose(4000);
            return redirect('/type');
        } catch (\Exception $e) {
            Alert::error('Error '.$e->getMessage(), 'Maaf!')->persistent('Close');
            return redirect('/type');      
        }
    }
}
