<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/search', 'HomeController@search_barang');
// Route::get('/{kategori}', 'HomeController@search_kategori');
Route::get('/search/{id}', 'HomeController@show');

Auth::routes();

Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');

Route::group(['prefix' => 'master_barang'], function(){
	Route::get('/', 'Master\MasterBarangController@index');
	Route::get('/json', 'Master\MasterBarangController@json');
	Route::get('/create', 'Master\MasterBarangController@create');
	Route::post('/save', 'Master\MasterBarangController@store');
	Route::get('/{id}', 'Master\MasterBarangController@edit');
	Route::post('/update/{id}', 'Master\MasterBarangController@update');
	Route::get('/delete/{id}', 'Master\MasterBarangController@destroy');
});

Route::group(['prefix' => 'kategori'], function(){
	Route::get('/', 'Master\MasterKategoriController@index');
	Route::get('/json', 'Master\MasterKategoriController@json');
	Route::post('/save', 'Master\MasterKategoriController@store');
	Route::get('/{id}', 'Master\MasterKategoriController@edit');
	Route::post('/update/{id}', 'Master\MasterKategoriController@update');
	Route::get('/delete/{id}', 'Master\MasterKategoriController@destroy');
});

Route::group(['prefix' => 'sub_kategori'], function(){
	Route::get('/', 'Master\MasterSubKategoriController@index');
	Route::get('/auto', 'Master\MasterSubKategoriController@auto_search');
	Route::get('/json', 'Master\MasterSubKategoriController@json');
	Route::post('/save', 'Master\MasterSubKategoriController@store');
	Route::get('/{id}', 'Master\MasterSubKategoriController@edit');
	Route::post('/update/{id}', 'Master\MasterSubKategoriController@update');
	Route::get('/delete/{id}', 'Master\MasterSubKategoriController@destroy');
});

Route::group(['prefix' => 'color'], function(){
	Route::get('/', 'Master\MasterColorController@index');
	Route::get('/json', 'Master\MasterColorController@json');
	Route::post('/save', 'Master\MasterColorController@store');
	Route::get('/{id}', 'Master\MasterColorController@edit');
	Route::post('/update/{id}', 'Master\MasterColorController@update');
	Route::get('/delete/{id}', 'Master\MasterColorController@destroy');
});

Route::group(['prefix' => 'model'], function(){
	Route::get('/', 'Master\MasterModelController@index');
	Route::get('/json', 'Master\MasterModelController@json');
	Route::post('/save', 'Master\MasterModelController@store');
	Route::get('/{id}', 'Master\MasterModelController@edit');
	Route::post('/update/{id}', 'Master\MasterModelController@update');
	Route::get('/delete/{id}', 'Master\MasterModelController@destroy');
});

Route::group(['prefix' => 'size'], function(){
	Route::get('/', 'Master\MasterSizeController@index');
	Route::get('/json', 'Master\MasterSizeController@json');
	Route::post('/save', 'Master\MasterSizeController@store');
	Route::get('/{id}', 'Master\MasterSizeController@edit');
	Route::post('/update/{id}', 'Master\MasterSizeController@update');
	Route::get('/delete/{id}', 'Master\MasterSizeController@destroy');
});

Route::group(['prefix' => 'type'], function(){
	Route::get('/', 'Master\MasterTypeController@index');
	Route::get('/json', 'Master\MasterTypeController@json');
	Route::post('/save', 'Master\MasterTypeController@store');
	Route::get('/{id}', 'Master\MasterTypeController@edit');
	Route::post('/update/{id}', 'Master\MasterTypeController@update');
	Route::get('/delete/{id}', 'Master\MasterTypeController@destroy');
});

Route::group(['prefix' => 'penjualan'], function(){
	Route::get('/', 'Transaction\PenjualanController@index');
	Route::get('/json', 'Transaction\PenjualanController@json');
	Route::get('/report', 'Transaction\PenjualanController@report');
	Route::get('/getreport', 'Transaction\PenjualanController@getreport');
	Route::get('/grafik', 'Transaction\PenjualanController@grafik');
	Route::get('/create', 'Transaction\PenjualanController@create');
	Route::post('/save', 'Transaction\PenjualanController@store');
	Route::get('/{id}', 'Transaction\PenjualanController@edit');
	Route::post('/update/{id}', 'Transaction\PenjualanController@update');
	Route::delete('/delete/{id}', 'Transaction\PenjualanController@delete');
});

Route::group(['prefix' => 'stok_barang'], function(){
	Route::get('/', 'Transaction\StokBarangController@index');
	Route::get('/json', 'Transaction\StokBarangController@json');
	Route::get('/create', 'Transaction\StokBarangController@create');
	Route::post('/save', 'Transaction\StokBarangController@store');
	Route::get('/search_barang', 'Transaction\StokBarangController@search_barang');
	Route::get('/search_kategori', 'Transaction\StokBarangController@search_kategori');
	Route::get('/search_color', 'Transaction\StokBarangController@search_color');
	Route::get('/search_model', 'Transaction\StokBarangController@search_model');
	Route::get('/search_size', 'Transaction\StokBarangController@search_size');
	Route::get('/search_type', 'Transaction\StokBarangController@search_type');
	Route::get('/view/{id}', 'Transaction\StokBarangController@show');
	Route::get('/{id}', 'Transaction\StokBarangController@edit');
	Route::post('/update/{id}', 'Transaction\StokBarangController@update');
	Route::delete('/delete/{id}', 'Transaction\StokBarangController@delete');
});