@extends('layouts.header')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Ubah Data Master Barang</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form action="{{ url('/master_barang/update/'.$m_barang->id) }}" method="POST" enctype="multipart/form-data" class="form-horizontal form-label-left">
                            @csrf
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kode_barang">Kode Barang 
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input value="{{ $m_barang->kode_barang }}" type="text" name="kode_barang" class="form-control {{ $errors->has('kode_barang') ? 'is-invalid':'' }}">
                                    <p class="text-danger">{{ $errors->first('kode_barang') }}</p>
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama_barang">Nama Barang</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input value="{{ $m_barang->nama_barang }}" type="text" name="nama_barang" class="form-control {{ $errors->has('nama_barang') ? 'is-invalid':'' }}">
                                    <p class="text-danger">{{ $errors->first('nama_barang') }}</p>
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="merek">Merek</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input value="{{ $m_barang->merek }}" type="text" name="merek" class="form-control {{ $errors->has('merek') ? 'is-invalid':'' }}">
                                    <p class="text-danger">{{ $errors->first('merek') }}</p>
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="keterangan">Keterangan</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <textarea id="editor1" name="keterangan" cols="10" rows="10" class="form-control {{ $errors->has('keterangan') ? 'is-invalid':'' }}">{{ $m_barang->keterangan }}</textarea>
                                    <p class="text-danger">{{ $errors->first('keterangan') }}</p>
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="additional_data1">Promo</label>
                                <div class="col-md-6 col-sm-6 ">
                                    Yes :<input type="radio" class="flat {{ $errors->has('additional_data1') ? 'is-invalid':'' }}" name="additional_data1" value="Yes" {{ $m_barang->additional_data1 == 'Yes' ? 'checked':'' }} /> 
                                    No :<input type="radio" class="flat {{ $errors->has('additional_data1') ? 'is-invalid':'' }}" name="additional_data1" value="No" {{ $m_barang->additional_data1 == 'No' ? 'checked':'' }} />
                                    <p class="text-danger">{{ $errors->first('additional_data1') }}</p>
                                </div>                                  
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" ></label>
                                <div class="col-md-6 col-sm-6 ">                                    
                                    <img src="{{ asset('uploads/images/'.$m_barang->image_path) }}" class="img-thumbnail"  style="max-width:200px;max-height:200px;float:left;" />
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="image">Pilih Gambar</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="file" name="image" />
                                    <input type="hidden" name="hidden_image" value="{{ $m_barang->image_path }}" />
                                    <p class="text-danger">{{ $errors->first('image') }}</p>
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <a href="{{ url('/master_barang') }}" class="btn btn-info btn-sm"><i class="fa fa-mail-reply"></i> Kembali</a>
                                <button class="btn btn-success btn-sm"><i class="fa fa-paper-plane"></i> Ubah</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showgambar').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    document.getElementById('')

    $("#inputgambar").change(function () {
        readURL(this);
    });
</script>
@endpush