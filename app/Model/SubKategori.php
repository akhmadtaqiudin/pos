<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SubKategori extends Model
{
    protected $table = 'm_sub_kategori';
    protected $guarded = [];
    protected $hidden = ['created_at','updated_at'];

    public function kategori(){
    	return $this->belongsTo('App\Model\Kategori','kode_kategori','kode_kategori');
    }
}
