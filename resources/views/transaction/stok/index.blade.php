@extends('layouts.header')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Stok Barang</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <a class="close-link btn btn-sm btn-info" href="{{ url('/stok_barang/create') }}" style="color: white;"><i class="fa fa-plus" title="Tambah Data"></i>Tambah Data</a>
                            </li>
                            <li>
                                <a class="collapse-link"><i class="fa fa-chevron-up" title="Tutup Layar"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <!-- <div class="well">
                            <div class="row">
                                <div class="col-md-4">
                                    Start Date
                                    <form class="form-horizontal">
                                        <fieldset>
                                            <div class="control-group">
                                                <div class="controls">
                                                    <div class="input-prepend input-group">
                                                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                        <input type="date" name="start_date" id="start_date" class="form-control datepicker-autoclose"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                                <div class="col-md-4">
                                    End Date
                                    <form class="form-horizontal">
                                        <fieldset>
                                            <div class="control-group">
                                                <div class="controls">
                                                    <div class="input-prepend input-group">
                                                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                        <input type="date" name="end_date" id="end_date" class="form-control datepicker-autoclose" />
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>                                
                                <div class="col-md-4">
                                    <button style="margin-top: 17px;" id="search" class="btn btn-info"><i class="fa fa-search"></i> Search</button>
                                </div>
                            </div>
                        </div> -->
                        <div class="row">                           
                            <table class="table table-hover table-bordered" style="width: 100%" id="table-stok">
                                <thead>
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>Image</th>
                                        <th>Kode Stok Barang</th>
                                        <th>Nama Barang</th>
                                        <th>Merek</th>
                                        <th>Kategori</th>
                                        <th>Jumlah Barang</th>
                                        <th>Harga Modal</th>
                                        <th>Harga Jual</th>
                                        <th>Warna</th>
                                        <th>Model</th>
                                        <th>Tipe</th>
                                        <th>Ukuran</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    $(document).ready(function (){
        // document.querySelector("#start_date").valueAsDate = new Date();
        // document.querySelector("#end_date").valueAsDate = new Date();
        
        // $.ajaxSetup({
        //     headers: {
        //        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //     }
        // });

        $('#table-stok').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: 'stok_barang/json',
            // ajax: {
            //     url: "{{ url('stok_barang/json') }}",
            //     type: 'GET',
            //     data: function (d) {
            //         d.start_date = $('#start_date').val();
            //         d.end_date = $('#end_date').val();
            //     }
            // },
            columns: [
                { data: 'created_at', name: 'created_at' },
                { data: 'image_path', name: 'image_path',
                    render:function(data,type,meta){
                        return '<img src="{{asset("uploads/images")}}'+"/"+''+data+'" width="100%" />';
                    }
                },
                { data: 'kode_stok_barang', name: 'kode_stok_barang' },
                { data: 'nama_barang', name: 'nama_barang' },
                { data: 'merek', name: 'merek' },
                { data: 'nama_kategori', name: 'nama_kategori' },
                { data: 'jumlah_barang', name: 'jumlah_barang' },
                { data: 'harga_modal', name: 'harga_modal' },
                { data: 'harga_jual', name: 'harga_jual' },
                { data: 'color', name: 'color' },
                { data: 'model', name: 'model' },
                { data: 'type', name: 'type' },
                { data: 'size', name: 'size' },
                { data: 'Aksi', name: 'Aksi' }
            ],
            columnDefs:[
                { "width": "30%", "targets": 1 }
            ]
        });
    });

    // $('#search').click(function(){
    //     $('#table-stok').DataTable().draw(true);
    // });

    function myConfirm() {
        var result = confirm("Yakin ingin menghapus data tersebut ?");
        if (result==true) {
            return true;
        } else {
            return false;
        }
    }
</script>
@endpush