<?php

namespace App\Http\Controllers\Master;

use App\Model\Kategori;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use Alert;

class MasterKategoriController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master/kategori.index');
    }

    public function json(){
        $kategori = Kategori::query();
        $kategori = $kategori->select('*');
        return Datatables::of($kategori)
            ->addColumn('Aksi', function($kategori){
                return  '<a href="'. url('/kategori/'.$kategori->id).'" style="margin:2px;" class="btn btn-warning btn-xs "><i class="fa fa-pencil"></i> Edit</a>'.
                    '<a onclick="return myConfirm();" href="'. url('/kategori/delete/'.$kategori->id).'" style="margin:2px;" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i> Delete</a>';
            })->rawColumns(['Aksi'])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,['nama_kategori' => 'required']);

        try {

            $kode = mt_rand();
            $kode = 'KAT'.$kode;
            
            $kategori = new Kategori();
            $kategori->kode_kategori = $kode;
            $kategori->nama_kategori = $request['nama_kategori'];
            $kategori->save();

            Alert::success('Data '.$kategori->nama_kategori.' Berhasil disimpan','Success')->autoclose(4000);
            return redirect('/kategori');
        } catch (\Exception $e) {
            Alert::error('Error '.$e->getMessage(), 'Maaf !')->persistent('Close');
            return redirect('/kategori');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function show(Kategori $kategori)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = Kategori::findOrFail($id);
        return view('master/kategori.edit',compact('kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,['nama_kategori' => 'required']);

        try {
            $kategori = Kategori::findOrFail($id);
            $kategori->kode_kategori = $request['kode_kategori'];
            $kategori->nama_kategori = $request['nama_kategori'];
            $kategori->update();

            Alert::success('Data '.$kategori->nama_kategori.' Berhasil diubah','Success')->autoclose(4000);
            return redirect('/kategori');
        } catch (\Exception $e) {
            Alert::error('Error '.$e->getMessage(), 'Maaf !')->persistent('Close');
            return redirect('/kategori/'.$id);  
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $kategori = Kategori::findOrFail($id);
            $kategori->delete();

            Alert::success('Kategori '.$kategori->nama_kategori.' berhasil dihapus', 'Success')->autoclose(4000);
            return redirect('/kategori');
        } catch (\Exception $e) {
            Alert::error('Error '.$e->getMessage(), 'Maaf!')->persistent('Close');
            return redirect('/kategori');      
        }
    }
}
