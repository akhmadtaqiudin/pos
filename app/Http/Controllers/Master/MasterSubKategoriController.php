<?php

namespace App\Http\Controllers\Master;

use App\Model\SubKategori;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Model\Kategori;
use DataTables;
use Alert;

class MasterSubKategoriController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master/subkategori.index');
    }

    public function json(){
        $sk = DB::table('m_sub_kategori')
            ->join('m_kategori', 'm_kategori.kode_kategori', '=', 'm_sub_kategori.kode_kategori')
            ->select('m_sub_kategori.*','m_kategori.nama_kategori as nama_kategori')
            ->get();
        
        return Datatables::of($sk)
            ->addColumn('Aksi', function($sk){
                return  '<a href="'. url('/sub_kategori/'.$sk->id).'" style="margin:2px;" class="btn btn-warning btn-xs "><i class="fa fa-pencil"></i> Edit</a>'.
                    '<a onclick="return myConfirm();" href="'. url('/sub_kategori/delete/'.$sk->id).'" style="margin:2px;" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i> Delete</a>';
            })->rawColumns(['Aksi'])->make(true);
    }

    public function auto_search(Request $request){
        $nama = $request->nama;
        $data = [];
        $data = Kategori::select('kode_kategori','nama_kategori')->where('nama_kategori','like','%'.$nama.'%')->get();
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,['nama_sub_kategori' => 'required']);

        try {

            $kode = mt_rand();
            $kode = 'KAT'.$kode;
            // $kategori = Kategori::create($request->all());
            $sk = new SubKategori();
            $sk->kode_sub_kategori = $kode;
            $sk->kode_kategori = $request['kode_kategori'];
            $sk->nama_sub_kategori = $request['nama_sub_kategori'];
            $sk->save();

            Alert::success('Sub Kategori '.$sk->nama_sub_kategori.' berhasil disimpan', 'Success')->autoclose(4000);
            return redirect('/sub_kategori');
        } catch (\Exception $e) {
            Alert::error('Error '.$e->getMessage(), 'Maaf !')->persistent('Close');
            return redirect('/sub_kategori');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\SubKategori  $subKategori
     * @return \Illuminate\Http\Response
     */
    public function show(SubKategori $subKategori)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\SubKategori  $subKategori
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sk = SubKategori::findOrFail($id);
        return view('master/subkategori.edit',compact('sk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\SubKategori  $subKategori
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,['nama_sub_kategori' => 'required']);

        try {
            $sk = SubKategori::findOrFail($id);
            $sk->kode_sub_kategori = $request['kode_sub_kategori'];
            $sk->kode_kategori = $request['kode_kategori'];
            $sk->nama_sub_kategori = $request['nama_sub_kategori'];
            $sk->update();

            Alert::success('Sub Kategori '.$sk->nama_sub_kategori.' berhasil diubah', 'Success')->autoclose(4000);
            return redirect('/sub_kategori');
        } catch (\Exception $e) {
            Alert::error('Error '.$e->getMessage(), 'Maaf!')->persistent('Close');
            return redirect('/sub_kategori/'.$id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\SubKategori  $subKategori
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $sk = SubKategori::findOrFail($id);
            $sk->delete();

            Alert::success('Sub Kategori '.$sk->nama_sub_kategori.' berhasil dihapus', 'Success')->autoclose(4000);
            return redirect('/sub_kategori');
        } catch (\Exception $e) {
            Alert::error('Error '.$e->getMessage(), 'Maaf!')->persistent('Close');
            return redirect('/sub_kategori');      
        }
    }
}
