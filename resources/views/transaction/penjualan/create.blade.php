@extends('layouts.header')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Tambah Data Penjualan</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form action="{{ url('/penjualan/save') }}" method="POST" class="form-horizontal form-label-left">
                            @csrf
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kode_stok_barang">Kode Stok Barang 
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control" name="kode_stok_barang">
                                        <option value="">Pilih</option>
                                        @foreach($stok as $data)
                                            <option value="{{ $data->kode_stok_barang }}">{{ $data->kode_stok_barang }}</option>
                                        @endforeach
                                    </select>
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="jumlah_barang">Jumlah Barang</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="text" name="jumlah_barang" class="form-control {{ $errors->has('jumlah_barang') ? 'is-invalid':'' }}">
                                    <p class="text-danger">{{ $errors->first('jumlah_barang') }}</p>
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <button class="btn btn-primary btn-sm"><i class="fa fa-paper-plane"></i> Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection