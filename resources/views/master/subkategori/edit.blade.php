@extends('layouts.header')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Edit Sub Kategori</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li>
                                <a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form action="{{ url('/sub_kategori/update/'.$sk->id) }}" method="POST"  class="form-horizontal">
                            @csrf
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="kode_sub_kategori">Kode Sub Kategori </label>
                                    </div>
                                    <div class="col-md-9">
                                         <input type="text" name="kode_sub_kategori" value="{{ $sk->kode_sub_kategori }}" class="form-control" readonly="true">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="nama_kategori">Nama Kategori</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="hidden" name="kode" value="{{ $sk->kode_kategori }}" id="kode">
                                        <input type="hidden" name="nama" value="{{ $sk->kategori->nama_kategori }}" id="nama">
                                        <select class="form-control" name="kode_kategori" id="select2" >{{ $sk->kode_kategori }}</select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="nama_sub_kategori">Nama Sub Kategori </label>
                                    </div>
                                    <div class="col-md-9">
                                         <input type="text" name="nama_sub_kategori" value="{{ $sk->nama_sub_kategori }}" class="form-control" >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <a href="{{ url('/sub_kategori') }}" class="btn btn-info btn-sm"><i class="fa fa-mail-reply"></i> Kembali</a>
                                <button class="btn btn-success btn-sm"><i class="fa fa-paper-plane"></i> Ubah</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">

    var opt = new Option($('#nama').val(),$('#kode').val(),true,true);
    $("#select2").append(opt).trigger('change');

    $(document).ready( function () {    
        $("#select2").select2({
            placeholder: 'Cari..',
            allowClear: true,
            ajax: {
                url: 'auto',
                dataType: 'json',
                delay: 250,
                processResults: function(data){
                    return{
                        results: $.map(data, function(nama){
                            return{
                                text: nama.nama_kategori,
                                id: nama.kode_kategori
                            }
                        })
                    };
                },
                cache:true
            }
        });
    });
</script>
@endpush
