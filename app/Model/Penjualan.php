<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Penjualan extends Model
{
    protected $table = 't_penjualan';
    protected $guarded = [];
    protected $hidden = ['created_at','updated_at'];
}
