@extends('layouts.header')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Size</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-3">
                                <form action="{{ url('/size/save') }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="size">Size</label>
                                        <input type="text" name="size" class="form-control {{ $errors->has('size') ? 'is-invalid':'' }}">
                                        <p class="text-danger">{{ $errors->first('size') }}</p>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary btn-sm"><i class="fa fa-paper-plane"></i> Simpan</button>
                                    </div>
                                </form>
                            </div>

                            <div class="col-md-9">
                                <table class="table table-hover table-bordered" style="width: 100%" id="table-size">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">No</th>
                                            <th>Size</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    $(function() {
        $('#table-size').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: 'size/json',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'size', name: 'size' },
                { data: 'Aksi', name: 'Aksi' }
            ]
        });
    });

    function myConfirm() {
        var result = confirm("Yakin ingin menghapus data tersebut ?");
        if (result==true) {
            return true;
        } else {
            return false;
        }
    }
</script>
@endpush