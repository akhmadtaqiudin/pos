<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MType extends Model
{
    protected $table = 'm_type';
    protected $guarded = [];
    protected $hidden = ['created_at','updated_at'];
}
