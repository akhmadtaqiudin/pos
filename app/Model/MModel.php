<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MModel extends Model
{
    protected $table = 'm_model';
    protected $guarded = [];
    protected $hidden = ['created_at','updated_at'];
}
