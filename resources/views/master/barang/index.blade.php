@extends('layouts.header')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Barang</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <a class="close-link btn btn-sm btn-info" href="{{ url('/master_barang/create') }}" style="color: white;"><i class="fa fa-plus" title="Tambah Data"></i>Tambah Data</a>
                            </li>
                            <li>
                                <a class="collapse-link"><i class="fa fa-chevron-up" title="Tutup Layar"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">                           
                            <table class="table table-hover table-bordered" style="width: 100%" id="table-barang">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;">No</th>
                                        <th>Image</th>
                                        <th>Kode Barang</th>
                                        <th>Nama Barang</th>
                                        <th>Merek</th>
                                        <th>Keterangan</th>
                                        <th>Promo</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    $(function() {
        $('#table-barang').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: 'master_barang/json',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'image_path', name: 'image_path',
                    render:function(data,type,meta){
                        return '<img src="{{asset("uploads/images")}}'+"/"+''+data+'" width="100%" />';
                    }
                },
                { data: 'kode_barang', name: 'kode_barang' },
                { data: 'nama_barang', name: 'nama_barang' },
                { data: 'merek', name: 'merek' },
                { data: 'keterangan', name: 'keterangan' },
                { data: 'additional_data1', name: 'additional_data1' },
                { data: 'Aksi', name: 'Aksi' }
            ],
            columnDefs:[
                { "width": "20%", "targets": 1 },
                { "height": "20px", "targets": 1 }
            ]
        });
    });

    function myConfirm() {
        var result = confirm("Yakin ingin menghapus data tersebut ?");
        if (result==true) {
            return true;
        } else {
            return false;
        }
    }
</script>
@endpush