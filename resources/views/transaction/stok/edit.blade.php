@extends('layouts.header')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Ubah Data Stok Barang</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form action="{{ url('/stok_barang/update/'.$sb->id) }}" method="POST" class="form-horizontal form-label-left">
                            @csrf
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" ></label>
                                <div class="col-md-6 col-sm-6 ">                                    
                                    <img src="{{ asset('uploads/images/'.$sb->image_path) }}" class="img-thumbnail"  style="max-width:200px;max-height:200px;float:left;" />
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kode_stok_barang">Kode Stok Barang 
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input value="{{ $sb->kode_stok_barang }}" type="text" name="kode_stok_barang" class="form-control {{ $errors->has('kode_stok_barang') ? 'is-invalid':'' }}" readonly="true">
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama_barang">Nama Barang</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input value="{{ $sb->nama_barang }}" type="text" name="nama_barang" class="form-control {{ $errors->has('nama_barang') ? 'is-invalid':'' }}" readonly="true">
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama_kategori">Kategori</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input value="{{ $sb->nama_kategori }}" type="text" name="nama_kategori" class="form-control {{ $errors->has('nama_kategori') ? 'is-invalid':'' }}" readonly="true">
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="color">Warna</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input value="{{ $sb->color }}" type="text" name="color" class="form-control {{ $errors->has('color') ? 'is-invalid':'' }}" readonly="true">
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="model">Model</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input value="{{ $sb->model }}" type="text" name="model" class="form-control {{ $errors->has('model') ? 'is-invalid':'' }}" readonly="true">
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="size" readonly="true">Ukuran</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input value="{{ $sb->size }}" type="text" name="size" class="form-control {{ $errors->has('size') ? 'is-invalid':'' }}" readonly="true">
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="type">Tipe</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input value="{{ $sb->type }}" type="text" name="type" class="form-control {{ $errors->has('type') ? 'is-invalid':'' }}" readonly="true">
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="jumlah_barang">Jumlah Barang</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input value="{{ $sb->jumlah_barang }}" type="text" name="jumlah_barang" class="form-control {{ $errors->has('jumlah_barang') ? 'is-invalid':'' }}">
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="harga_modal">Harga Modal</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input value="{{ $sb->harga_modal }}" type="text" name="harga_modal" class="form-control {{ $errors->has('harga_modal') ? 'is-invalid':'' }}">
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="harga_jual">Harga Jual</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input value="{{ $sb->harga_jual }}" type="text" name="harga_jual" class="form-control {{ $errors->has('harga_jual') ? 'is-invalid':'' }}">
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <a href="{{ url('/stok_barang') }}" class="btn btn-info btn-sm"><i class="fa fa-mail-reply"></i> Kembali</a>
                                <button class="btn btn-success btn-sm"><i class="fa fa-paper-plane"></i> Ubah</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection