@extends('layouts.header')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Laporan Penjualan</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="well">
                            <div class="row">
                                <div class="col-md-4">
                                    Start Date
                                    <form class="form-horizontal">
                                        <fieldset>
                                            <div class="control-group">
                                                <div class="controls">
                                                    <div class="input-prepend input-group">
                                                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                        <input type="date" name="start_date" id="start_date" class="form-control datepicker-autoclose"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                                <div class="col-md-4">
                                    End Date
                                    <form class="form-horizontal">
                                        <fieldset>
                                            <div class="control-group">
                                                <div class="controls">
                                                    <div class="input-prepend input-group">
                                                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                        <input type="date" name="end_date" id="end_date" class="form-control datepicker-autoclose" />
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>                                
                                <div class="col-md-4">
                                    <button style="margin-top: 17px;" id="search" class="btn btn-info"><i class="fa fa-search"></i> Search</button>
                                </div>
                            </div>
                        </div>

                        <table class="table table-hover table-bordered" style="width: 100%" id="report_table">
                            <thead>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Image</th>
                                    <th>Kode Stok Barang</th>
                                    <th>Nama Barang</th>
                                    <th>Merek</th>
                                    <th>Kategori</th>
                                    <th>Jumlah Barang</th>
                                    <th>Warna</th>
                                    <th>Model</th>
                                    <th>Tipe</th>
                                    <th>Ukuran</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('js/jszip.min.js') }}"></script>
<script src="{{ asset('vendor/pdfmake/build/pdfmake.min.js') }}"></script>
<script src="{{ asset('vendor/pdfmake/build/vfs_fonts.js') }}"></script>
<script src="{{ asset('vendor/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('vendor/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('vendor/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script>
    $(document).ready( function () {
        document.querySelector("#start_date").valueAsDate = new Date();
        document.querySelector("#end_date").valueAsDate = new Date();
        $.ajaxSetup({
            headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#report_table').DataTable({
            processing: true,
            serverSide: true,
        
        dom: 'Bfrtip',
        buttons: [
            'csv', 'excel', 'pdf', 'print'
        ],
            ajax: {
                url: "{{ url('penjualan/getreport') }}",
                type: 'GET',
                data: function (d) {
                    d.start_date = $('#start_date').val();
                    d.end_date = $('#end_date').val();
                }
            },
            columns: [
                { data: 'created_at', name: 'created_at' },
                { data: 'image_path', name: 'image_path',
                    render:function(data,type,meta){
                        return '<img src="{{asset("uploads/images")}}'+"/"+''+data+'" width="100%" />';
                    }
                },
                { data: 'kode_stok_barang', name: 'kode_stok_barang' },
                { data: 'nama_barang', name: 'nama_barang' },
                { data: 'merek', name: 'merek' },
                { data: 'nama_kategori', name: 'nama_kategori' },
                { data: 'jumlah_barang', name: 'jumlah_barang' },
                { data: 'color', name: 'color' },
                { data: 'model', name: 'model' },
                { data: 'type', name: 'type' },
                { data: 'size', name: 'size' }
            ],
            columnDefs:[
                { "width": "30%", "targets": 1 }
            ]
        });
    });

    $('#search').click(function(){
        $('#report_table').DataTable().draw(true);
    });
</script>
@endpush