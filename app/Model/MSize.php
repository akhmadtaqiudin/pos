<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MSize extends Model
{
    protected $table = 'm_size';
    protected $guarded = [];
    protected $hidden = ['created_at','updated_at'];
}
