<?php

namespace App\Http\Controllers\Transaction;

use App\Model\Penjualan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Model\StokBarang;
use App\Model\Barang;
use Alert;
use Charts;
use DataTables;

class PenjualanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('transaction/penjualan.index');
    }

    public function json(){
        $sb = DB::table('t_penjualan')
            ->join('t_stok_barang', 't_stok_barang.kode_stok_barang', '=', 't_penjualan.kode_stok_barang')
            ->join('m_barang', 'm_barang.kode_barang', '=', 't_stok_barang.kode_barang')
            ->join('m_kategori', 'm_kategori.kode_kategori', '=', 't_stok_barang.kode_kategori')
            ->join('m_detail','m_detail.kode_detail', '=','t_stok_barang.kode_detail')
            ->join('m_color','m_color.id', '=','m_detail.kode_color')
            ->join('m_model','m_model.id', '=','m_detail.kode_model')
            ->join('m_size','m_size.id', '=','m_detail.kode_size')
            ->join('m_type','m_type.id', '=','m_detail.kode_type')
            ->select('t_penjualan.*','m_barang.image_path as image_path','m_barang.nama_barang as nama_barang','m_barang.merek as merek','m_kategori.nama_kategori as nama_kategori','m_color.color as color','m_model.model as model','m_size.size as size','m_type.type as type')
            ->get();

            return Datatables::of($sb)
            // ->addColumn('Aksi', function($sb){
            //     return  '<a href="'. url('/stok_barang/'.$sb->id).'" style="margin:2px;" class="btn btn-warning btn-xs "><i class="fa fa-pencil"></i> Edit</a>'.
            //         '<a onclick="return myConfirm();" href="'. url('/stok_barang/delete/'.$sb->id).'" style="margin:2px;" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i> Delete</a>';
            // })
            ->editColumn('created_at', function($sb){
                return date('d-m-Y', strtotime($sb->created_at) );
            })->make(true);
            // ->rawColumns(['Aksi'])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $stok = StokBarang::all();
        return view('transaction/penjualan.create',compact('stok'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'kode_stok_barang' => 'required',
            'jumlah_barang' => 'required'
        ]);

        try {
            $id = $request['kode_stok_barang'];
            $jumlah = $request['jumlah_barang'];
            $stok = DB::table('t_stok_barang')->where('kode_stok_barang', $id)->first();
            $id_sb = $stok->id;

            if($jumlah >= $stok->jumlah_barang){
                Alert::error('Maaf ! Jumlah yang anda masukan melebihi dari stok yang ada')->persistent('Close');
                return redirect('/penjualan/create');
            }

            $input_jumlah = ($stok->jumlah_barang - $jumlah);

            $stok_barang = StokBarang::findOrFail($id_sb);
            $stok_barang->kode_stok_barang = $request['kode_stok_barang'];
            $stok_barang->jumlah_barang = $input_jumlah;
            $stok_barang->update();

            Penjualan::create($request->all());

            Alert::success('Data '.$request->kode_stok_barang.' Berhasil disimpan','Success')->autoclose(4000);
            return redirect('/penjualan');
        } catch (\Exception $e) {
            Alert::error('Error '.$e->getMessage(), 'Maaf !')->persistent('Close');
            return redirect('/penjualan/create');
        }      
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Penjualan  $penjualan
     * @return \Illuminate\Http\Response
     */
    public function show(Penjualan $penjualan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Penjualan  $penjualan
     * @return \Illuminate\Http\Response
     */
    public function edit(Penjualan $penjualan)
    {
        return view('transaction/penjualan.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Penjualan  $penjualan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Penjualan $penjualan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Penjualan  $penjualan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Penjualan $penjualan)
    {
        //
    }

    public function grafik(){
 
        $penjualan = Penjualan::where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),date('Y'))->get();
        $chart = Charts::database($penjualan, 'bar', 'highcharts')
                  ->title("Grafik Penjualan Barang Dalam Setahun")
                  ->elementLabel("Penjualan Barang")
                  ->dimensions(1000, 500)
                  ->responsive(false)
                  ->groupByMonth(date('Y'), true);
        return view('transaction/penjualan.grafik',compact('chart')); 
    }

    public function report(){
        return view('transaction/penjualan.report');
    }

    public function getreport(){
 
        $penjualan = Penjualan::query();

        $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
        $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');
        if($start_date && $end_date){
            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));

            $penjualan->whereRaw("date(t_penjualan.created_at) >= '".$start_date."' AND date(t_penjualan.created_at) <= '".$end_date."'");
        }

        $penjualan = $penjualan
            ->join('t_stok_barang', 't_stok_barang.kode_stok_barang', '=', 't_penjualan.kode_stok_barang')
            ->join('m_barang', 'm_barang.kode_barang', '=', 't_stok_barang.kode_barang')
            ->join('m_kategori', 'm_kategori.kode_kategori', '=', 't_stok_barang.kode_kategori')
            ->join('m_detail','m_detail.kode_detail', '=','t_stok_barang.kode_detail')
            ->join('m_color','m_color.id', '=','m_detail.kode_color')
            ->join('m_model','m_model.id', '=','m_detail.kode_model')
            ->join('m_size','m_size.id', '=','m_detail.kode_size')
            ->join('m_type','m_type.id', '=','m_detail.kode_type')
            ->select('t_penjualan.created_at as created_at','t_penjualan.kode_stok_barang as kode_stok_barang','t_penjualan.jumlah_barang as jumlah_barang','m_barang.image_path as image_path','m_barang.nama_barang as nama_barang','m_barang.merek as merek','m_kategori.nama_kategori as nama_kategori','m_color.color as color','m_model.model as model','m_size.size as size','m_type.type as type')
            ->get();
        return Datatables::of($penjualan)
            ->editColumn('created_at', function($penjualan){
                return date('d-m-Y', strtotime($penjualan->created_at) );
            })->make(true);
    }
}
