@extends('layouts.header')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Tambah Data Master Barang</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form action="{{ url('/master_barang/save') }}" method="POST" enctype="multipart/form-data" class="form-horizontal form-label-left">
                            @csrf
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kode_barang">Kode Barang 
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="text" name="kode_barang" class="form-control {{ $errors->has('kode_barang') ? 'is-invalid':'' }}">
                                    <p class="text-danger">{{ $errors->first('kode_barang') }}</p>
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama_barang">Nama Barang</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="text" name="nama_barang" class="form-control {{ $errors->has('nama_barang') ? 'is-invalid':'' }}">
                                    <p class="text-danger">{{ $errors->first('nama_barang') }}</p>
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="merek">Merek</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="text" name="merek" class="form-control {{ $errors->has('merek') ? 'is-invalid':'' }}">
                                    <p class="text-danger">{{ $errors->first('merek') }}</p>
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="keterangan">Keterangan</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <textarea id="editor1" name="keterangan" cols="10" rows="10" class="form-control {{ $errors->has('keterangan') ? 'is-invalid':'' }}"></textarea>
                                    <p class="text-danger">{{ $errors->first('keterangan') }}</p>
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="additional_data1">Promo</label>
                                <div class="col-md-6 col-sm-6 ">
                                    Yes :
                                    <input type="radio" class="flat {{ $errors->has('additional_data1') ? 'is-invalid':'' }}" name="additional_data1" value="Yes"/> No :
                                    <input type="radio" class="flat {{ $errors->has('additional_data1') ? 'is-invalid':'' }}" name="additional_data1" value="No" checked="" />
                                    <p class="text-danger">{{ $errors->first('additional_data1') }}</p>
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="image">Pilih Gambar</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="file" name="image">
                                    <p class="text-danger">{{ $errors->first('image') }}</p>
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <button class="btn btn-primary btn-sm"><i class="fa fa-paper-plane"></i> Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection