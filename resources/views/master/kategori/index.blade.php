@extends('layouts.header')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Kategori</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-3">
                                @if(session('error'))
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        {{ session('error') }}
                                    </div>
                                @endif

                                <form action="{{ url('/kategori/save') }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="nama_kategori">Kategori</label>
                                        <input type="text" name="nama_kategori" class="form-control {{ $errors->has('nama_kategori') ? 'is-invalid':'' }}">
                                        <p class="text-danger">{{ $errors->first('nama_kategori') }}</p>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary btn-sm"><i class="fa fa-paper-plane"></i> Simpan</button>
                                    </div>
                                </form>
                            </div>

                            <div class="col-md-9">
                                @if (session('success'))
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        {!! session('success') !!}
                                    </div>
                                @endif
                                
                                <table class="table table-hover table-bordered" style="width: 100%" id="table-kategori">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">No</th>
                                            <th>Kode Kategori</th>
                                            <th>Nama Kategori</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    $(function() {
        $('#table-kategori').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: 'kategori/json',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'kode_kategori', name: 'kode_kategori' },
                { data: 'nama_kategori', name: 'nama_kategori' },
                { data: 'Aksi', name: 'Aksi' }
            ]
        });
    });

    function myConfirm() {
        var result = confirm("Yakin ingin menghapus data tersebut ?");
        if (result==true) {
            return true;
        } else {
            return false;
        }
    }
</script>
@endpush