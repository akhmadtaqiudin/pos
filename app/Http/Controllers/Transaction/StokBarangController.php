<?php

namespace App\Http\Controllers\Transaction;

use App\Model\StokBarang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Model\Kategori;
use App\Model\MColor;
use App\Model\MDetail;
use App\Model\MModel;
use App\Model\MSize;
use App\Model\MType;
use App\Model\Barang;
use DataTables;
use Alert;

class StokBarangController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('transaction/stok.index');
    }

    public function json(){
        $sb = DB::table('t_stok_barang')
            ->join('m_barang', 'm_barang.kode_barang', '=', 't_stok_barang.kode_barang')
            ->join('m_kategori', 'm_kategori.kode_kategori', '=', 't_stok_barang.kode_kategori')
            ->join('m_detail','m_detail.kode_detail', '=','t_stok_barang.kode_detail')
            ->join('m_color','m_color.id', '=','m_detail.kode_color')
            ->join('m_model','m_model.id', '=','m_detail.kode_model')
            ->join('m_size','m_size.id', '=','m_detail.kode_size')
            ->join('m_type','m_type.id', '=','m_detail.kode_type')
            ->select('t_stok_barang.*','m_barang.image_path as image_path','m_barang.nama_barang as nama_barang','m_barang.merek as merek','m_kategori.nama_kategori as nama_kategori','m_color.color as color','m_model.model as model','m_size.size as size','m_type.type as type')
            ->get();

        return Datatables::of($sb)
            ->addColumn('Aksi', function($sb){
                return  '<a href="'. url('/stok_barang/'.$sb->id).'" style="margin:2px;" class="btn btn-warning btn-xs "><i class="fa fa-pencil"></i> Edit</a>'.
                    '<a onclick="return myConfirm();" href="'. url('/stok_barang/delete/'.$sb->id).'" style="margin:2px;" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i> Delete</a>';
            })
            ->editColumn('created_at', function($sb){
                return date('d-m-Y', strtotime($sb->created_at) );
            })
            ->rawColumns(['Aksi'])->make(true);
    }

    public function search_barang(Request $request){
        $nama = $request->nama;
        $data_barang = [];
        $data_barang = Barang::select('kode_barang','nama_barang')->where('nama_barang','like','%'.$nama.'%')->get();
        return response()->json($data_barang);
    }

    public function search_kategori(Request $request){
        $nama = $request->nama;
        $data_kategori = [];
        $data_kategori = Kategori::select('kode_kategori','nama_kategori')->where('nama_kategori','like','%'.$nama.'%')->get();
        return response()->json($data_kategori);
    }

    public function search_color(Request $request){
        $color = $request->color;
        $data_color = [];
        $data_color = MColor::select('id','color')->where('color','like','%'.$color.'%')->get();
        return response()->json($data_color);
    }

    public function search_model(Request $request){
        $model = $request->model;
        $data_model = [];
        $data_model = MModel::select('id','model')->where('model','like','%'.$model.'%')->get();
        return response()->json($data_model);
    }

    public function search_size(Request $request){
        $size = $request->size;
        $data_size = [];
        $data_size = MSize::select('id','size')->where('size','like','%'.$size.'%')->get();
        return response()->json($data_size);
    }

    public function search_type(Request $request){
        $type = $request->type;
        $data_type = [];
        $data_type = MType::select('id','type')->where('type','like','%'.$type.'%')->get();
        return response()->json($data_type);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('transaction/stok.created');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'kode_stok_barang' => 'required',
            'jumlah_barang' => 'required',
            'harga_modal' => 'required|min:3',
            'harga_jual' => 'required|min:3'
        ]);

        try {

            $acak = rand(0, 9999);
            $kode = $request['kode_stok_barang'];
            $kode = $kode.$acak;

            $detail = new MDetail();
            $detail->kode_detail = $acak;
            $detail->kode_color = $request['kode_color'];
            $detail->kode_size = $request['kode_size'];
            $detail->kode_model = $request['kode_model'];
            $detail->kode_type = $request['kode_type'];
            $detail->save();
            
            $sb = new StokBarang;
            $sb->kode_stok_barang = $kode;
            $sb->kode_barang = $request['kode_barang'];
            $sb->kode_kategori = $request['kode_kategori'];
            $sb->kode_detail = $acak;
            $sb->jumlah_barang = $request['jumlah_barang'];
            $sb->harga_modal = $request['harga_modal'];
            $sb->harga_jual = $request['harga_jual'];
            $sb->save();

            Alert::success('Data '.$sb->kode_stok_barang.' Berhasil disimpan','Success')->autoclose(4000);
            return redirect('/stok_barang');
        } catch (\Exception $e) {
            Alert::error('Error '.$e->getMessage(), 'Maaf !')->persistent('Close');
            return redirect('/stok_barang/create');
        }            
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\StokBarang  $stokBarang
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sk = StokBarang::findOrFail($id);
        return view('transaction/stok.show',compact('sk'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\StokBarang  $stokBarang
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sb = DB::table('t_stok_barang')
            ->join('m_barang', 'm_barang.kode_barang', '=', 't_stok_barang.kode_barang')
            ->join('m_kategori', 'm_kategori.kode_kategori', '=', 't_stok_barang.kode_kategori')
            ->join('m_detail','m_detail.kode_detail', '=','t_stok_barang.kode_detail')
            ->join('m_color','m_color.id', '=','m_detail.kode_color')
            ->join('m_model','m_model.id', '=','m_detail.kode_model')
            ->join('m_size','m_size.id', '=','m_detail.kode_size')
            ->join('m_type','m_type.id', '=','m_detail.kode_type')
            ->select('t_stok_barang.*','m_barang.image_path as image_path','m_barang.nama_barang as nama_barang','m_kategori.nama_kategori as nama_kategori','m_color.color as color','m_model.model as model','m_size.size as size','m_type.type as type')
            ->where('t_stok_barang.id', $id)->first();
        return view('transaction/stok.edit',compact('sb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\StokBarang  $stokBarang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate($request,[
            'jumlah_barang' => 'required',
            'harga_modal' => 'required|min:3',
            'harga_jual' => 'required|min:3'
        ]);

        try {
            $sb = StokBarang::findOrFail($id);
            $sb->jumlah_barang = $request['jumlah_barang'];
            $sb->harga_modal = $request['harga_modal'];
            $sb->harga_jual = $request['harga_jual'];
            $sb->update();

            Alert::success('Data '.$sb->kode_stok_barang.' Berhasil diubah','Success')->autoclose(4000);
            return redirect('/stok_barang');
        } catch (\Exception $e) {
            Alert::error('Error '.$e->getMessage(), 'Maaf !')->persistent('Close');
            return redirect('/stok_barang/'.$id);  
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\StokBarang  $stokBarang
     * @return \Illuminate\Http\Response
     */
    public function destroy(StokBarang $stokBarang)
    {
        //
    }
}
