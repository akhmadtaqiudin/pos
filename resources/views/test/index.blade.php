@extends('layouts.app')
@section('content')
    <table class="table table-bordered table-striped">
 <tr>
  <th width="10%">Image</th>
  <th width="35%">First Name</th>
  <th width="35%">Last Name</th>
  <th width="30%">Action</th>
 </tr>
 @foreach($data as $row)
  <tr>
   <td><img src="{{ asset('uploads/images/'.$row->image_path) }}" class="img-thumbnail" width="75" /></td>
   <td>{{ $row->nama_barang }}</td>
   <td>{{ $row->merek }}</td>
   <td>
    <a href="{{ url('/gallery/'.$row->id) }}" style="margin:2px;" class="btn btn-warning btn-xs "><i class="fa fa-pencil"></i> Edit</a>
   </td>
  </tr>
 @endforeach
</table>
{!! $data->links() !!}
@endsection