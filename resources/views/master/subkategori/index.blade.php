@extends('layouts.header')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Sub Kategori</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-3">
                                <form action="{{ url('/sub_kategori/save') }}" method="POST">
                                    @csrf
                                <div class="form-group">
                                    <label for="kode_kategori">Kategori</label>
                                    <select class="form-control" name="kode_kategori" id="select2" ></select>
                                </div>
                                    <div class="form-group">
                                        <label for="nama_sub_kategori">Nama Sub Kategori</label>
                                        <input type="text" name="nama_sub_kategori" class="form-control {{ $errors->has('nama_sub_kategori') ? 'is-invalid':'' }}">
                                        <p class="text-danger">{{ $errors->first('nama_sub_kategori') }}</p>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary btn-sm"><i class="fa fa-paper-plane"></i> Simpan</button>
                                    </div>
                                </form>
                            </div>

                            <div class="col-md-9">
                                <table class="table table-hover table-bordered" style="width: 100%" id="table-kategori">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">No</th>
                                            <th>Kode Sub Kategori</th>
                                            <th>Nama Kategori</th>
                                            <th>Nama Sub Kategori</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    $(function() {
        $('#table-kategori').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: 'sub_kategori/json',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'kode_sub_kategori', name: 'kode_sub_kategori' },
                { data: 'nama_kategori', name: 'nama_kategori' },
                { data: 'nama_sub_kategori', name: 'nama_sub_kategori' },
                { data: 'Aksi', name: 'Aksi' }
            ]
        });
    });

    function myConfirm() {
        var result = confirm("Yakin ingin menghapus data tersebut ?");
        if (result==true) {
            return true;
        } else {
            return false;
        }
    }

    $("#select2").select2({
        placeholder: 'Cari..',
        ajax: {
            url: 'sub_kategori/auto',
            dataType: 'json',
            delay: 250,
            processResults: function(data){
                return{
                    results: $.map(data, function(nama){
                        return{
                            text: nama.nama_kategori,
                            id: nama.kode_kategori
                        }
                    })
                };
            },
            cache:true
        }
    });
</script>
@endpush