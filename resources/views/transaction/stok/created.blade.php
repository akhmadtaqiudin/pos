@extends('layouts.header')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Tambah Data Stok Barang</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form action="{{ url('/stok_barang/save') }}" method="POST" class="form-horizontal form-label-left">
                            @csrf
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kode_stok_barang">Kode Stok Barang 
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="text" name="kode_stok_barang" class="form-control {{ $errors->has('kode_stok_barang') ? 'is-invalid':'' }}">
                                    <p class="text-danger">{{ $errors->first('kode_stok_barang') }}</p>
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kode_barang">Nama Barang</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control" name="kode_barang" id="kb" ></select>
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kode_kategori">Kategori</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control" name="kode_kategori" id="kat" ></select>
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kode_color">Warna</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control" name="kode_color" id="warna" ></select>
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kode_model">Model</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control" name="kode_model" id="model" ></select>
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kode_size">Ukuran</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control" name="kode_size" id="ukuran" ></select>
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kode_type">Tipe</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control" name="kode_type" id="tipe" ></select>
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="jumlah_barang">Jumlah Barang</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="text" name="jumlah_barang" class="form-control {{ $errors->has('jumlah_barang') ? 'is-invalid':'' }}">
                                    <p class="text-danger">{{ $errors->first('jumlah_barang') }}</p>
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="harga_modal">Harga Modal</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="text" name="harga_modal" class="form-control {{ $errors->has('harga_modal') ? 'is-invalid':'' }}">
                                    <p class="text-danger">{{ $errors->first('harga_modal') }}</p>
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="harga_jual">Harga Jual</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="text" name="harga_jual" class="form-control {{ $errors->has('harga_jual') ? 'is-invalid':'' }}">
                                    <p class="text-danger">{{ $errors->first('harga_jual') }}</p>
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <button class="btn btn-primary btn-sm"><i class="fa fa-paper-plane"></i> Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    $("#kb").select2({
        placeholder: 'Cari..',
        ajax: {
            url: 'search_barang',
            dataType: 'json',
            delay: 250,
            processResults: function(data){
                return{
                    results: $.map(data, function(nama){
                        return{
                            text: nama.nama_barang,
                            id: nama.kode_barang
                        }
                    })
                };
            },
            cache:true
        }
    });

    $("#kat").select2({
        placeholder: 'Cari..',
        ajax: {
            url: 'search_kategori',
            dataType: 'json',
            delay: 250,
            processResults: function(data){
                return{
                    results: $.map(data, function(nama){
                        return{
                            text: nama.nama_kategori,
                            id: nama.kode_kategori
                        }
                    })
                };
            },
            cache:true
        }
    });

    $("#warna").select2({
        placeholder: 'Cari..',
        ajax: {
            url: 'search_color',
            dataType: 'json',
            delay: 250,
            processResults: function(data){
                return{
                    results: $.map(data, function(color){
                        return{
                            text: color.color,
                            id: color.id
                        }
                    })
                };
            },
            cache:true
        }
    });

    $("#ukuran").select2({
        placeholder: 'Cari..',
        ajax: {
            url: 'search_size',
            dataType: 'json',
            delay: 250,
            processResults: function(data){
                return{
                    results: $.map(data, function(size){
                        return{
                            text: size.size,
                            id: size.id
                        }
                    })
                };
            },
            cache:true
        }
    });

    $("#model").select2({
        placeholder: 'Cari..',
        ajax: {
            url: 'search_model',
            dataType: 'json',
            delay: 250,
            processResults: function(data){
                return{
                    results: $.map(data, function(model){
                        return{
                            text: model.model,
                            id: model.id
                        }
                    })
                };
            },
            cache:true
        }
    });

    $("#tipe").select2({
        placeholder: 'Cari..',
        ajax: {
            url: 'search_type',
            dataType: 'json',
            delay: 250,
            processResults: function(data){
                return{
                    results: $.map(data, function(type){
                        return{
                            text: type.type,
                            id: type.id
                        }
                    })
                };
            },
            cache:true
        }
    });
</script>
@endpush

