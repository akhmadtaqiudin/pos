<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StokBarang extends Model
{
    protected $table = 't_stok_barang';
    protected $guarded = [];
    // protected $hidden = ['created_at','updated_at'];

    public function barang(){
    	return $this->belongsTo('App\Model\Barang','kode_barang','kode_barang');
    }

    public function kategori(){
    	return $this->belongsTo('App\Model\Kategori','kode_kategori','kode_kategori');
    }

    public function mdetail(){
    	return $this->belongsTo('App\Model\MDetail','kode_detail','kode_detail');
    }
}
