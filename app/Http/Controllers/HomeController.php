<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\StokBarang;
use App\Model\Barang;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {    
        $ca = Barang::where('additional_data1','Yes')->orderBy('created_at','DESC')->first();
        $param = $ca->id;

        $ca2 = Barang::where('additional_data1','Yes')->whereNotIn('id', [$param])->orderBy('created_at','DESC')->get();

        $barang = DB::table('t_stok_barang')
            ->join('m_barang', 'm_barang.kode_barang', '=', 't_stok_barang.kode_barang')
            ->select('t_stok_barang.*','m_barang.image_path as image_path','m_barang.nama_barang as nama_barang')
            ->paginate(24);

        return view('welcome',compact('barang','ca','ca2'));
    }

    public function search_barang(Request $request){
        $nama = $request->nama_barang;
        $ca = Barang::where('additional_data1','Yes')->orderBy('created_at','DESC')->first();
        $param = $ca->id;

        $ca2 = Barang::where('additional_data1','Yes')->whereNotIn('id', [$param])->orderBy('created_at','DESC')->get();

        $barang = DB::table('t_stok_barang')
            ->join('m_barang', 'm_barang.kode_barang', '=', 't_stok_barang.kode_barang')
            ->select('t_stok_barang.*','m_barang.image_path as image_path','m_barang.nama_barang as nama_barang')
            ->where('nama_barang','like','%'.$nama.'%')->paginate(24);

        return view('welcome',compact('barang','ca','ca2'));
    }

    public function search_kategori($request){
        
        $ca = Barang::where('additional_data1','Yes')->orderBy('created_at','DESC')->first();
        $param = $ca->id;

        $ca2 = Barang::where('additional_data1','Yes')->whereNotIn('id', [$param])->orderBy('created_at','DESC')->get();

        $barang = DB::table('t_stok_barang')
            ->join('m_barang', 'm_barang.kode_barang', '=', 't_stok_barang.kode_barang')
            ->join('m_kategori', 'm_kategori.kode_kategori', '=', 't_stok_barang.kode_kategori')
            ->select('t_stok_barang.*','m_barang.image_path as image_path','m_barang.nama_barang as nama_barang','m_kategori.nama_kategori as nama_kategori')
            ->where('nama_kategori','like','%'.$request.'%')->paginate(24);
        
        return view('welcome',compact('barang','ca','ca2'));
    }

   /* public function show($id){
        
        $barang = DB::table('t_stok_barang')
            ->join('m_barang', 'm_barang.kode_barang', '=', 't_stok_barang.kode_barang')
            ->join('m_kategori', 'm_kategori.kode_kategori', '=', 't_stok_barang.kode_kategori')
            ->select('t_stok_barang.*','m_barang.image_path as image_path','m_barang.nama_barang as nama_barang','m_kategori.nama_kategori as nama_kategori')
            ->where('kode_stok_barang',$request)->get();
        
        return view('detail',compact('barang'));
    }*/

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard()
    {
        return view('home');
    }

    public function create()
    {
        return view('test.create');
    }

    // untuk test
    public function store(Request $request)
    {
        $request->validate([
            'image_path'         =>  'required|image'
        ]);

        $image = $request->file('image');

        $new_name = rand() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('uploads/images'), $new_name);
        $form_data = array(
            'nama_barang'  =>   $request->nama_barang,
            'merek'        =>   $request->merek,
            'image_path'   =>   $new_name
        );

        Barang::create($form_data);

        return redirect('laravel-gallery')->with('success', 'Data Added successfully.');
    }

    public function show($id)
    {
        $data = Barang::findOrFail($id);
        return view('test.view', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Barang::findOrFail($id);
        return view('test.form', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image_name = $request->hidden_image;
        $image = $request->file('image');
        if($image != '')
        {
            $request->validate([
                'nama_barang'    =>  'required',
                'merek'     =>  'required',
                'image_path'         =>  'image|max:2048'
            ]);

            $image_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('uploads/images'), $image_name);
        }
        else
        {
            $request->validate([
                'nama_barang'    =>  'required',
                'merek'     =>  'required'
            ]);
        }

        $form_data = array(
            'nama_barang'       =>   $request->nama_barang,
            'merek'        =>   $request->merek,
            'image_path'            =>   $image_name
        );
  
        // dd($image_name.' = '.$image);exit();
        Barang::whereId($id)->update($form_data);

        return redirect('gallery')->with('success', 'Data is successfully updated');
    }
}
