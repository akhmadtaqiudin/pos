@extends('layouts.header')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Tambah Data Stok Barang</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form action="{{ url('/stok_barang') }}" class="form-horizontal form-label-left">
                            @csrf
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kode_stok_barang">Kode Stok Barang 
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="text" value="{{$sk->kode_stok_barang}}" class="form-control" readonly="true">
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kode_barang">Kode Barang</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="text" class="form-control" value="{{$sk->barang->nama_barang }}" readonly="true">
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kode_kategori">Kategori</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="text" class="form-control" value="{{$sk->kode_kategori }}" readonly="true">
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kode_color">Warna</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="text" class="form-control" value="{{$sk->mdetail->mcolor->color }}" readonly="true">
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kode_model">Model</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="text" class="form-control" value="{{$sk->mdetail->mmodel->model }}" readonly="true">
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kode_size">Ukuran</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="text" class="form-control" value="{{$sk->mdetail->msize->size }}" readonly="true">
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kode_type">Tipe</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="text" class="form-control" value="{{$sk->mdetail->mtype->type }}" readonly="true">
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="jumlah_barang">Jumlah Barang</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="text" value="{{$sk->jumlah_barang }}" class="form-control">
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="harga_modal">Harga Modal</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="text" value="{{$sk->harga_modal }}" class="form-control">
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="harga_jual">Harga Jual</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="text" value="{{$sk->harga_jual }}" class="form-control">
                                </div>                                
                            </div>
                            <div class="item form-group">
                                <button class="btn btn-info btn-sm"><i class="fa fa-mail-reply"></i> Kembali</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection