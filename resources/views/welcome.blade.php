<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>{{ config('app.name', 'Toko') }}</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->        
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    </head>
    <body>
        <header>
            <nav class="navbar fixed-top navbar-dark bg-dark shadow-sm">
                <div class="container">
                    <a href="{{ url('/') }}" class="navbar-brand">
                        <img src="{{ asset('images/solata-2.png') }}" style="width: 58%;">
                    </a>
                    <ul>
                        <li><a href=""></a>Info</li>
                        <li><a href=""></a>Kontak</li>
                    </ul>

                    <form class="form-inline" action="{{ url('/search') }}" method="GET">
                        <input name="nama_barang" class="form-control mr-sm-2" type="text" placeholder="Nama Produk" aria-label="Nama Produk">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Cari</button>
                    </form>
                </div>            
            </nav>
        </header>

        <main role="main">       
            <div class="container">
                <div class="box-carousel">
                    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                            @foreach($ca2 as $data)
                                <li data-target="#carouselExampleCaptions" data-slide-to="{{ asset('uploads/images/'.$data->id) }}"></li>
                            @endforeach
                        </ol>

                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="{{ asset('uploads/images/'.$ca->image_path) }}" class="d-block w-100" alt="..."  />
                                <div class="carousel-caption d-none d-md-block">
                                    <!-- <h5>Gambar Slide Yang Pertama</h5>
                                    <p>Gambar pemandangan sungai.</p> -->
                                </div>
                            </div>
                            @foreach($ca2 as $data)
                                <div class="carousel-item">
                                    <img src="{{ asset('uploads/images/'.$data->image_path) }}" class="d-block w-100" alt="..."  />
                                    <div class="carousel-caption d-none d-md-block">
                                        <!-- <h5>Gambar Slide Yang Kedua</h5>
                                        <p>Gambar pemandangan sawah di desa.</p> -->
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            <!-- end slide -->

                <div class="row">
                    <div class="col-sm-6 col-md-4 col-lg-3 mt-4">
                        <a href="{{ url('/tactical') }}"><img class="category-section" src="{{ asset('images/tactical.png') }}" width="100%"></a>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3 mt-4">
                        <a href="{{ url('/tni') }}"><img class="category-section" src="{{ asset('images/tni.png') }}" width="100%"></a>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3 mt-4">
                        <a href="{{ url('/polisi') }}"><img class="category-section" src="{{ asset('images/polisi.png') }}" width="100%"></a>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3 mt-4">
                        <a href="{{ url('/security') }}"><img class="category-section" src="{{ asset('images/security.png') }}" width="100%"></a>
                    </div>
                </div>

                <div class="row">
                    @foreach($barang as $data)
                        <div class="col-sm-6 col-md-4 col-lg-3 mt-4">
                            <div class="card">
                                <a href="{{ url('/search/'.$data->kode_stok_barang) }}">
                                    <img class="card-img-top" src="{{ asset('uploads/images/'.$data->image_path) }}">
                                    <div class="card-body">
                                        <p class="card-text">{{ $data->nama_barang }}</p>
                                        <p class="card-text">Rp. {{ number_format($data->harga_jual, 2) }}</p>              
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </main>

        <footer class="text-muted">
            <div class="container">
                <div class="pag">
                    {{ $barang->links() }}
                </div>
                
                <p class="float-right">
                    <a href="#">Back to top</a>
                </p>
            </div>
        </footer>
        
        <script src="{{ asset('js/jquery-3.4.1.slim.min.js') }}" ></script>
        <script src="{{ asset('js/popper.min.js') }}" ></script>
        <script src="{{ asset('js/bootstrap-4.4.1.min.js') }}" ></script>
    </body>
</html>
