<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MDetail extends Model
{
    protected $table = 'm_detail';
    protected $guarded = [];
    protected $hidden = ['created_at','updated_at'];

    public function mcolor(){
    	return $this->belongsTo('App\Model\MColor','kode_color','id');
    }

    public function mmodel(){
    	return $this->belongsTo('App\Model\MModel','kode_model','id');
    }

    public function msize(){
    	return $this->belongsTo('App\Model\MSize','kode_size','id');
    }

    public function mtype(){
    	return $this->belongsTo('App\Model\MType','kode_type','id');
    }
}
