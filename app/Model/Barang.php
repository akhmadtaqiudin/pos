<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'm_barang';
    protected $guarded = [];
    protected $hidden = ['created_at','updated_at'];

    public function kategori(){
    	return $this->belongsTo('App\Model\Kategori','kode_kategori','kode_kategori');
    }

    // public function subkategori(){
    // 	return $this->belongsTo('App\Model\SubKategori','kode_kategori','kode_kategori');
    // }
}
