<?php

namespace App\Http\Controllers\Master;

use App\Model\MModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use Alert;

class MasterModelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master/model.index');
    }

    public function json(){
        $mm = MModel::query();
        $mm = $mm->select('*');
        return Datatables::of($mm)
            ->addColumn('Aksi', function($mm){
                return  '<a href="'. url('/model/'.$mm->id).'" style="margin:2px;" class="btn btn-warning btn-xs "><i class="fa fa-pencil"></i> Edit</a>'.
                    '<a onclick="return myConfirm();" href="'. url('/model/delete/'.$mm->id).'" style="margin:2px;" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i> Delete</a>';
            })->rawColumns(['Aksi'])->make(true); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,['model' => 'required']);

        try {
            $mm = MModel::create($request->all());

            Alert::success('Model '.$mm->model.' berhasil ditambahkan', 'Success')->autoclose(4000);
            return redirect('/model');
        } catch (\Exception $e) {
            Alert::error('Error '.$e->getMessage(), 'Maaf!')->persistent('Close');
            return redirect('/model');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\MModel  $mModel
     * @return \Illuminate\Http\Response
     */
    public function show(MModel $mModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\MModel  $mModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mm = MModel::findOrFail($id);
        return view('master/model.edit',compact('mm'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\MModel  $mModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,['model' => 'required']);

        try {
            $mm = MModel::findOrFail($id);
            $mm->update($request->all());

            Alert::success('Model '.$mm->model.' berhasil diubah', 'Success')->autoclose(4000);
            return redirect('/model');
        } catch (\Exception $e) {
            Alert::error('Error '.$e->getMessage(), 'Maaf!')->persistent('Close');
            return redirect('/model/edit');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\MModel  $mModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $mm = MModel::findOrFail($id);
            $mm->delete();

            Alert::success('Model '.$mm->model.' berhasil dihapus', 'Success')->autoclose(4000);
            return redirect('/model');
        } catch (\Exception $e) {
            Alert::error('Error '.$e->getMessage(), 'Maaf!')->persistent('Close');
            return redirect('/model');      
        }
    }
}
