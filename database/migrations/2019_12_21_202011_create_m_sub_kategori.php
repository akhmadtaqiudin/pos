<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMSubKategori extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_sub_kategori', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_sub_kategori');
            $table->string('kode_kategori');
            $table->string('nama_sub_kategori');
            $table->string('additional_data1')->nullable();
            $table->string('additional_data2')->nullable();
            $table->integer('additional_data3')->nullable();
            $table->integer('additional_data4')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_sub_kategori');
    }
}
