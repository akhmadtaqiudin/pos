->nullable()<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTStokBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_stok_barang', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_stok_barang');
            $table->string('kode_barang');
            $table->string('kode_kategori');
            $table->string('kode_detail');
            $table->integer('jumlah_barang');
            $table->decimal('harga_modal', 8, 1);            
            $table->decimal('harga_jual', 8, 1);
            $table->string('additional_data1')->nullable();
            $table->string('additional_data2')->nullable();
            $table->integer('additional_data3')->nullable();
            $table->integer('additional_data4')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_stok_barang');
    }
}
